# Bardings
A mod that adds in horse bardings for every kingdom and a few more.

**Note that you need the client-side mod BardingzzClient
for this to work properly. Read more on its [GitLab page](https://gitlab.com/tyoda-wurm/BardingzzClient).**

Credit goes to Berkle and Olette for all the textures.


![](bardings.png)

![](bardings_game.png)
