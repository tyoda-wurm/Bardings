/*
Bardings mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.bardings;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.ItemTemplatesCreatedListener;
import org.gotti.wurmunlimited.modloader.interfaces.Versioned;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import org.gotti.wurmunlimited.modsupport.items.ModItems;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * A mod that adds in horse bardings for every kingdom and a few more.
 */
public class Bardings implements WurmServerMod, ItemTemplatesCreatedListener, Versioned, Configurable {
    public static Logger logger = Logger.getLogger(Bardings.class.getName());
    public static final String version = "1.1";

    /**
     * Whether the bardings should be craftable
     */
    public static boolean bardingsCraftable = true;

    @Override
    public void configure(Properties p) {
        bardingsCraftable = Boolean.parseBoolean(p.getProperty("bardingsCraftable", String.valueOf(bardingsCraftable)));
    }

    @Override
    public void onItemTemplatesCreated() {
        ModItems.init();
        logger.info("Adding bardings.");
        BardingsFactory.createBarding("jenn", BardingTypes.BARDING_TYPE_COTTON,  "Jenn-Kellon barding");
        BardingsFactory.createBarding("molr", BardingTypes.BARDING_TYPE_COTTON,  "Mol-Rehan barding");
        BardingsFactory.createBarding("hots", BardingTypes.BARDING_TYPE_COTTON,  "Horde of the Summoned barding");
        BardingsFactory.createBarding("zjen", BardingTypes.BARDING_TYPE_COTTON,  "Dragon Kingdom barding");
        BardingsFactory.createBarding("empi", BardingTypes.BARDING_TYPE_COTTON,  "Empire of Mol-Rehan barding");
        BardingsFactory.createBarding("blac", BardingTypes.BARDING_TYPE_COTTON,  "Black Legion barding");
        BardingsFactory.createBarding("ebon", BardingTypes.BARDING_TYPE_COTTON,  "Ebonaura barding");
        BardingsFactory.createBarding("king", BardingTypes.BARDING_TYPE_COTTON,  "Kingdom of Sol barding");
        BardingsFactory.createBarding("ther", BardingTypes.BARDING_TYPE_COTTON,  "The Roman Republic barding");
        BardingsFactory.createBarding("mace", BardingTypes.BARDING_TYPE_COTTON,  "Macedonia barding");
        BardingsFactory.createBarding("drea", BardingTypes.BARDING_TYPE_COTTON,  "Dreadnought barding");
        BardingsFactory.createBarding("thec", BardingTypes.BARDING_TYPE_COTTON,  "The Crusaders barding");
        BardingsFactory.createBarding("pand", BardingTypes.BARDING_TYPE_COTTON,  "Pandemonium barding");
        BardingsFactory.createBarding("legi", BardingTypes.BARDING_TYPE_COTTON,  "Legion of Anubis barding");
        BardingsFactory.createBarding("wurm", BardingTypes.BARDING_TYPE_COTTON,  "Wurm University barding");
        BardingsFactory.createBarding("yval", BardingTypes.BARDING_TYPE_COTTON,  "Valhalla Descendants barding");
        BardingsFactory.createBarding("apoc", BardingTypes.BARDING_TYPE_COTTON,  "Apocalypse Order barding");
        BardingsFactory.createBarding("abra", BardingTypes.BARDING_TYPE_COTTON,  "Abralon barding");
        BardingsFactory.createBarding("comm", BardingTypes.BARDING_TYPE_COTTON,  "The Commonwealth barding");
        BardingsFactory.createBarding("valh", BardingTypes.BARDING_TYPE_COTTON,  "Valhalla barding");
        BardingsFactory.createBarding("free", BardingTypes.BARDING_TYPE_COTTON,  "Freedom barding");
        logger.info("Added all bardings.");
    }

    @Override
    public String getVersion() {
        return version;
    }
}