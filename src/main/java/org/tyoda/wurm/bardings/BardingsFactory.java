/*
Bardings mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.bardings;

import com.wurmonline.server.behaviours.BehaviourList;
import com.wurmonline.server.combat.ArmourTemplate;
import com.wurmonline.server.items.*;
import com.wurmonline.server.skills.SkillList;
import com.wurmonline.shared.constants.IconConstants;
import org.gotti.wurmunlimited.modsupport.ItemTemplateBuilder;

import java.io.IOException;
import java.util.logging.Level;

public class BardingsFactory {
    /**
     * Creates a new barding item template. Also creates a creation entry if
     * bardingsCraftable is true in {@link  Bardings}
     * @param identifier The four letter unique identifier for the barding.
     * @param type The material type of the barding
     * @param name The name of the barding
     */
    public static void createBarding(String identifier, BardingTypes type, String name){
        String modelName = "mod.bardingzz.tyoda.bardings."+identifier+".";

        ItemTemplateBuilder builder = new ItemTemplateBuilder(modelName)
            .descriptions("strong", "well-made", "ok", "fragile")
            .name(name, "bardings", "A simple, yet functional cloth barding that will provide moderate protection.")
            .itemTypes(new short[]{
                    ItemTypes.ITEM_TYPE_REPAIRABLE,
                    ItemTypes.ITEM_TYPE_MISSION,
                    ItemTypes.ITEM_TYPE_CLOTH,
                    ItemTypes.ITEM_TYPE_CREATURE_WEARABLE,
                    ItemTypes.ITEM_TYPE_ARMOUR,
                    ItemTypes.ITEM_TYPE_NAMED
            })
            .imageNumber((short)IconConstants.ICON_ARMOR_BARDING)
            .behaviourType(BehaviourList.itemBehaviour)
            .combatDamage(0)
            .decayTime(3024000L)
            .dimensions(40, 50, 60)
            .primarySkill(-10)
            .bodySpaces(new byte[]{2})
            .modelName(modelName)
            .difficulty(40.0F)
            .weightGrams(12000)
            .material(Materials.MATERIAL_COTTON)
            .value(10000);

        if(type == BardingTypes.BARDING_TYPE_LEATHER){
            builder.name(name, "bardings", "A leather barding fitted together with metal rings and rivets.")
                .itemTypes(new short[]{
                        ItemTypes.ITEM_TYPE_REPAIRABLE,
                        ItemTypes.ITEM_TYPE_MISSION,
                        ItemTypes.ITEM_TYPE_LEATHER,
                        ItemTypes.ITEM_TYPE_CREATURE_WEARABLE,
                        ItemTypes.ITEM_TYPE_ARMOUR,
                        ItemTypes.ITEM_TYPE_NAMED,
                        ItemTypes.ITEM_TYPE_COLORABLE
                })
                .weightGrams(15000)
                .material(Materials.MATERIAL_LEATHER)
                .value(20000);
        } else if(type == BardingTypes.BARDING_TYPE_CHAIN){ // chain
            builder.name(name, "bardings", "A powerful chain barding to protect your noble steed.")
                .itemTypes(new short[]{
                        ItemTypes.ITEM_TYPE_REPAIRABLE,
                        ItemTypes.ITEM_TYPE_MISSION,
                        ItemTypes.ITEM_TYPE_METAL,
                        ItemTypes.ITEM_TYPE_CREATURE_WEARABLE,
                        ItemTypes.ITEM_TYPE_ARMOUR,
                        ItemTypes.ITEM_TYPE_NAMED,
                        ItemTypes.ITEM_TYPE_COLORABLE
                })
                .difficulty(60.0F)
                .weightGrams(25000)
                .material(Materials.MATERIAL_IRON)
                .value(50000);
        }
        ItemTemplate template = null;
        try{
            template = builder.build();
        } catch (IOException e){
            Bardings.logger.log(Level.SEVERE, "Failed while creating item "+name, e);
        }

        if(template != null){
            new ArmourTemplate(template.getTemplateId(), ArmourTemplate.ARMOUR_TYPE_CLOTH, 0.1F);
            if(Bardings.bardingsCraftable) {
                switch (type) {
                    case BARDING_TYPE_COTTON:
                        CreationEntryCreator.createSimpleEntry(SkillList.CLOTHTAILORING, ItemList.needleIron, ItemList.clothYard, template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.ANIMAL_EQUIPMENT);
                        CreationEntryCreator.createSimpleEntry(SkillList.CLOTHTAILORING, ItemList.needleCopper, ItemList.clothYard, template.getTemplateId(), false, true, 0.0F, false, false, CreationCategories.ANIMAL_EQUIPMENT);
                        break;
                    case BARDING_TYPE_LEATHER:
                        AdvancedCreationEntry bardingLeather = CreationEntryCreator.createAdvancedEntry(SkillList.LEATHERWORKING, ItemList.metalRivet, ItemList.leather, template.getTemplateId(), false, false, 0.0F, true, false, CreationCategories.ANIMAL_EQUIPMENT);
                        bardingLeather.addRequirement(new CreationRequirement(1, ItemList.leather, 4, true));
                        bardingLeather.addRequirement(new CreationRequirement(2, ItemList.metalRivet, 50, true));
                        break;
                    case BARDING_TYPE_CHAIN:
                        CreationEntryCreator.createSimpleEntry(SkillList.SMITHING_ARMOUR_CHAIN, ItemList.anvilLarge, ItemList.armourChains, template.getTemplateId(), false, true, 10.0F, false, false, CreationCategories.ANIMAL_EQUIPMENT);
                        break;
                }
            }
        }
    }
}
